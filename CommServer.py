#!/bin/python
import traceback
from flask import Flask
from flask import request, Response
from flask import jsonify
from flask import render_template, redirect, url_for
import v1_0_0
import firebase_admin
from firebase_admin import credentials
from firebase_admin import auth
from json import dumps
import datetime
from time import sleep
from flask import abort

import openpyxl
import smtplib
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email.mime.base import MIMEBase

APP_LINK_IOS = "https://itunes.apple.com/us/app/serveup/id1434643983"
APP_LINK_ANDROID = "https://play.google.com/store/apps/details?id=org.kgvdevelopment.serveup"

app = Flask(__name__)

ERROR_MSG = "Fuck you gtfo of our app you fucker"
def onValueError():
    print("requesting new token before interaction...")
    abort(401)


if (not len(firebase_admin._apps)):
    cred = credentials.Certificate("/home/ubuntu/sjscommservSecretKey.json")
    default_app = firebase_admin.initialize_app(cred)

@app.route("/privatepolicy")
def privatepolicy():
    return render_template("privatepolicy.html")
@app.route("/download")
def downloadApp():
    return render_template("download.html", ios_dl=APP_LINK_IOS, android_dl=APP_LINK_ANDROID)
@app.route("/download/now")
def downloadAppInstant():
    browser = request.user_agent.browser
    version = request.user_agent.version and int(request.user_agent.version.split('.')[0])
    platform = request.user_agent.platform
    uas = request.user_agent.string    
    if browser and version:
        if (platform == 'android'):
            return redirect(APP_LINK_ANDROID)
        if (platform == 'iphone' or platform == 'ipad' or platform == 'ipod'):
            return redirect(APP_LINK_IOS)

    return redirect(url_for('downloadApp'))
    
@app.route("/<string:version>/getHours", methods=["POST"])
def getHours(version):
    if version == "v1.0.0":
        uid = ""
        uname = ""
        try:
            decoded_token = auth.verify_id_token(request.authorization["password"])
            uid = decoded_token["email"]
#            if uid[-8:] != "@sjs.org":
#                abort(401)
            uname = decoded_token["name"]
            whoTheySayTheyAre = v1_0_0.sqlMethods().validate(uid)
        except ValueError:
            onValueError()
        except Exception as e:
            traceback.print_exc()
            abort(401)

        requestReceived = request.get_json(force = True)
        requestReceived["fromID"] = v1_0_0.sqlMethods().getUserIDFromEmail(decoded_token["email"])

        return v1_0_0.Runner().getHours(requestReceived, decoded_token)

@app.route("/<string:version>/getHoursSuper", methods=["POST"])
def getHoursSuper(version):
    if version == "v1.0.0":
        uid = ""
        uname = ""
        try:
            decoded_token = auth.verify_id_token(request.authorization["password"])
            uid = decoded_token["email"]
#            if uid[-8:] != "@sjs.org":
#                abort(401)
            uname = decoded_token["name"]
            whoTheySayTheyAre = v1_0_0.sqlMethods().validate(uid)
        except ValueError:
            onValueError()
        except Exception as e:
            traceback.print_exc()
            abort(401)

        requestReceived = request.get_json(force = True)
        requestReceived["fromID"] = v1_0_0.sqlMethods().getUserIDFromEmail(decoded_token["email"])

        return v1_0_0.Runner().getHoursSuper(requestReceived, decoded_token)

@app.route("/<string:version>/lock/get", methods=["POST"])
def getLock(version):
    if version == "v1.0.0":
        uid = ""
        uname = ""
        try:
            decoded_token = auth.verify_id_token(request.authorization["password"])
            uid = decoded_token["email"]
#            if uid[-8:] != "@sjs.org":
#                abort(401)
            uname = decoded_token["name"]
            whoTheySayTheyAre = v1_0_0.sqlMethods().validate(uid)
        except ValueError:
            onValueError()
        except Exception as e:
            traceback.print_exc()
            abort(401)

        requestReceived = request.get_json(force = True)
        requestReceived["user_id"] = v1_0_0.sqlMethods().getUserIDFromEmail(decoded_token["email"])

        return dumps(v1_0_0.sqlMethods().lockStatus(requestReceived["user_id"], requestReceived["event_id"]))

@app.route("/<string:version>/lock/release", methods=["POST"])
def meemsreleaseLock(version):
    if version == "v1.0.0":
        uid = ""
        uname = ""
        try:
            decoded_token = auth.verify_id_token(request.authorization["password"])
            uid = decoded_token["email"]
#            if uid[-8:] != "@sjs.org":
#                abort(401)
            uname = decoded_token["name"]
            whoTheySayTheyAre = v1_0_0.sqlMethods().validate(uid)
        except ValueError:
            onValueError()
        except Exception as e:
            traceback.print_exc()
            abort(401)

        requestReceived = request.get_json(force = True)
        requestReceived["user_id"] = v1_0_0.sqlMethods().getUserIDFromEmail(decoded_token["email"])
        
        return dumps(v1_0_0.sqlMethods().releaseLock(requestReceived["user_id"], requestReceived["event_id"]))

@app.route("/<string:version>/update", methods=["GET", "POST"])
def serverUpdate(version): #Returns list of new events and people
    if version == "v1.0.0":
        uid = ""
        uname = ""
        try:
            decoded_token = auth.verify_id_token(request.authorization["password"])
            uid = decoded_token["email"]
#            if uid[-8:] != "@sjs.org":
#                abort(401)
            uname = decoded_token["name"]
            whoTheySayTheyAre = v1_0_0.sqlMethods().validate(uid)
        except ValueError:
            onValueError()
        except Exception as e:
            traceback.print_exc()
            abort(401)

        requestReceived = request.get_json(force = True)
        print("received: " + str(requestReceived))

        if whoTheySayTheyAre == "INVALID":
            #add this user
            v1_0_0.sqlMethods().addUserWithEmail(uid, uname)
            print("Added new user",
                    v1_0_0.sqlMethods().getUserWithUserID(v1_0_0.sqlMethods().getUserIDFromEmail(uid)).email)

        a = v1_0_0.Runner().serverUpdate(requestReceived, decoded_token)
#        print("response: " + str(a))
        return(a)
