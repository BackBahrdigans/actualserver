import traceback

from sqlMethods import sqlMethods
from CommServEvent import CommServEvent
from CommServPerson import CommServPerson
from Location import Location
from secret import *

from json import dumps
import datetime
from time import sleep

import smtplib
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email.mime.base import MIMEBase
from email import encoders

class Runner:
    def getHours(self, requestReceived, decoded_token):

        print("received request for individual hours table")
        #requestReceived["user_id"] = sqlMethods().getUserIDFromEmail(decoded_token["email"])
        requestReceived["fromID"] = sqlMethods().getUserIDFromEmail(decoded_token["email"])
        if not sqlMethods().getUserFromUserID(requestReceived["fromID"], requestReceived["fromID"]).isOfficer and requestReceived["fromID"] != requestReceived["user_id"]:
            return "False"

        table = sqlMethods().getHoursTable(requestReceived["user_id"])
        table = table[:-1]

        import openpyxl
        from openpyxl.writer.excel import save_virtual_workbook as save_wb
        wb=openpyxl.Workbook()
        ws_write = wb.active
        for row in table:
            ws_write.append(row)
        name = "excel/" + sqlMethods().getUserFromUserID(requestReceived["user_id"], requestReceived["user_id"]).name + " " + str(datetime.datetime.now())
        #wb.save(filename=name+'.xlsx')
        temp = save_wb(wb)
        
        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        server.ehlo()
        server.login(GMAIL_USER,GMAIL_PASS)
        
        msg = MIMEMultipart()
        msg['Subject'] = "Hours Report" 
        msg['From'] = "serveup.dev@gmail.com"
        msg['To'] = decoded_token["email"]

        part = MIMEBase('application', "octet-stream")
        #part.set_payload(open(name + ".xlsx", "rb").read())
        part.set_payload(temp)
        encoders.encode_base64(part)

        part.add_header('Content-Disposition', 'attachment; filename="' + sqlMethods().getUserFromUserID(requestReceived["user_id"], requestReceived["user_id"]).name + '.xlsx"')

        msg.attach(part)
        
        server.sendmail("serveup.dev@gmail.com", decoded_token["email"], msg.as_string())
        server.close()
        print("sending hours report for " + sqlMethods().getUserFromUserID(requestReceived["user_id"], requestReceived["user_id"]).name + " to " + decoded_token["email"])
        return dumps(table)

    def getHoursSuper(self, requestReceived, decoded_token):
        print("received request for hours super table")
        requestReceived["user_id"] = sqlMethods().getUserIDFromEmail(decoded_token["email"])
        requestReceived["fromID"] = sqlMethods().getUserIDFromEmail(decoded_token["email"])

        if not sqlMethods().getUserFromUserID(requestReceived["fromID"], requestReceived["fromID"]).isOfficer and requestReceived["fromID"] != requestReceived["user_id"]:
            return "False"

        table = sqlMethods().getHoursSuperTable(requestReceived["user_id"])

        import openpyxl
        from openpyxl.writer.excel import save_virtual_workbook as save_wb
        wb=openpyxl.Workbook()
        for person in table:
            ws_write = wb.create_sheet(person[-1])
            for row in person[:-1]:
                ws_write.append(row)
        name = "excel/" + sqlMethods().getUserFromUserID(requestReceived["user_id"], requestReceived["user_id"]).name + " " + str(datetime.datetime.now())
        #wb.save(filename=name+'.xlsx')
        temp = save_wb(wb)

        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        server.ehlo()
        server.login(GMAIL_USER,GMAIL_PASS)
        
        msg = MIMEMultipart()
        msg['Subject'] = "Hours Report" 
        msg['From'] = "serveup.dev@gmail.com"
        msg['To'] = decoded_token["email"]

        part = MIMEBase('application', "octet-stream")
        part.set_payload(temp)
        encoders.encode_base64(part)

        part.add_header('Content-Disposition', 'attachment; filename="hours.xlsx"')

        msg.attach(part)
        
        server.sendmail("serveup.dev@gmail.com", decoded_token["email"], msg.as_string())
        server.close()
        print("sending hours super table to " + decoded_token["email"])
        return dumps(table)
 
    def serverUpdate(self, requestReceived, decoded_token):
        requestReceived["user_id"] = sqlMethods().getUserIDFromEmail(decoded_token["email"])
        print("received request for updates (user_id: %d, update_id: %d)" % (int(requestReceived["user_id"]), int(requestReceived["update_id"])))

        #if request.method == "POST":
        data = sqlMethods().getNewData(int(requestReceived["update_id"]), int(requestReceived["user_id"]))
            
        receivedData = requestReceived['data']
        events = []
        people = []
        update = {"update_id" : 0, "events" : [], "people" : [], "user_id" : requestReceived["user_id"]}
        failedUpdate = {"update_id" : 0, "events" : [], "people" : [], "user_id" : requestReceived["user_id"]}
        newEvents = []
        for event in receivedData['events']:
            asdf = CommServEvent()
            asdf = asdf.jsonEventToObject(event)
            if asdf.identity == -1:
                updateID = sqlMethods().addEvent(asdf, requestReceived["user_id"])
                if updateID != -1:
                    update["update_id"] = updateID["update_id"]
                    update["events"].append(updateID["object"])
                    newEvents.append(updateID["object"]["id"])
                else:
                    failedUpdate["events"].append(asdf.serialize())
            else:
                if sqlMethods().upToDate(asdf.identity, 1, int(requestReceived["update_id"])):
                    updateID = sqlMethods().updateEvent(asdf, int(requestReceived["user_id"]))
                    if updateID != -1:
                        update["events"].append(updateID["object"])
                        update["update_id"] = updateID["update_id"]
                    else:
                        print("up to date")
                        failedUpdate["events"].append(asdf.serialize())
                else:
                    failedUpdate["events"].append(asdf.serialize())
            # these arrays are unnecessary if you are just going to operate one by one, bc you can just do it here with CommServEvent().jsonPersonToObject(event)
        for person in receivedData['people']:
            asdf = CommServPerson()
            asdf = asdf.jsonPersonToObject(person)
            if asdf.identity == -1:
                updateID = sqlMethods().addUser(asdf, newEvents)
                update["update_id"] = updateID["update_id"]
                update["people"].append(updateID["object"])
            else:
                if sqlMethods().upToDate(asdf.identity, 0, int(requestReceived["update_id"])):
                    updateID = sqlMethods().updateUser(asdf, int(requestReceived["user_id"]), newEvents)
                    if updateID != -1:
                        update["update_id"] = updateID["update_id"]
                        update["people"].append(updateID["object"])
                    else:
                        failedUpdate["people"].append(asdf.serialize())
                else:
                        failedUpdate["people"].append(asdf.serialize())
        data["failedData"] = {"people" : failedUpdate["people"], "events" : failedUpdate["events"]}
        data["clientData"] = {"people" : update["people"], "events" : update["events"]}
        data["update_id"] = sqlMethods().getUpdateID()
        print("submitting updates " + str(requestReceived["update_id"]) + " --> " + str(data["update_id"]))
        
        return dumps(data)
