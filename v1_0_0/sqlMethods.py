import sqlite3
import datetime
import json
from CommServPerson import CommServPerson
from CommServEvent import CommServEvent
from Location import Location

from os.path import join, dirname, abspath
DB_FILE = join(dirname(dirname(abspath(__file__))), 'data.db')

class sqlMethods:
    def getUserWithUserID(self, user_id):
        #this is stupid but it just makes everything easier
        return self.getUserFromUserID(user_id, user_id)

    def officer(self, email):
        conn = sqlite3.connect(DB_FILE)
        c = conn.cursor()
        sql = "SELECT * FROM autoOfficers WHERE email = ?;"
        args = (email,)
        c.execute(sql, args)
        output = c.fetchone()

        if output is None:
            return 0

        else:
            return 2

    def getUserFromUserID(self, queried_id, user_id):
        conn = sqlite3.connect(DB_FILE)
        c = conn.cursor()
        sql = "SELECT JSON, rowid FROM users WHERE rowid = ?;"
        args = (queried_id,)
        c.execute(sql, args)
        output = c.fetchone()
        conn.close()

        userSerialized = json.loads(output[0])
        user = CommServPerson()
        user = user.jsonPersonToObject(userSerialized)
        user.eventsSignedUpFor = user.eventsSignedUpFor
        user.eventsConfirmedFor = user.eventsConfirmedFor
        user.birthday = user.birthday
        if queried_id == user_id or self.getUserFromUserID(user_id, user_id).isOfficer:
            pass
        else:
            newThing = {}
            for event_id in user.eventsConfirmedFor.keys():
                event = self.getEventFromEventID(int(event_id))
                if user_id in event.leaders:
                    newThing[event_id] = user.eventsConfirmedFor[event_id]
                    print("adding " + event_id + " to payload for user " + str(user_id))
            user.eventsConfirmedFor = newThing

            user.studentID = None
            user.birthday = None
            user.phone = None
        return user

    def updateUser(self, user, user_id, newEvents):

        for dayEvent in user.eventsSignedUpFor.keys():
            if int(dayEvent) < 0:
                import copy
                days = copy.deepcopy(user.eventsSignedUpFor[dayEvent])
                user.eventsSignedUpFor[str(newEvents[-1 * int(dayEvent) - 1])] = days 
                del user.eventsSignedUpFor[dayEvent]

        for dayEvent in user.eventsConfirmedFor.keys():
            if int(dayEvent) < 0:
                import copy
                days = copy.deepcopy(user.eventsConfirmedFor[dayEvent])
                user.eventsConfirmedFor[str(newEvents[-1 * int(dayEvent) - 1])] = days 
                del user.eventsConfirmedFor[dayEvent]

        if self.getUserFromUserID(user_id, user_id).isOfficer and user.email != "INVALID":
            officerLevel = self.getUserFromUserID(user_id, user_id).isOfficer
            if officerLevel < 2:
                user.isOfficer = self.getUserFromUserID(user.identity, user.identity).isOfficer

            conn = sqlite3.connect(DB_FILE)
            c = conn.cursor()
            sql = "UPDATE users SET JSON = ? WHERE rowid = ?"
            args = (json.dumps(user.serialize()), user.identity)
            c.execute(sql, args)
            conn.commit()
            conn.close()
            return self.insertUpdate(user.identity, False, user.serialize())

        elif user.identity == user_id:
            original = self.getUserFromUserID(user.identity, user.identity)
            user.isOfficer = original.isOfficer
            user.email = original.email
            user.identity = original.identity
            for event_id in user.eventsConfirmedFor.keys():
                if user.identity in self.getEventFromEventID(int(event_id)).leaders:
                    original.eventsConfirmedFor[event_id] = user.eventsConfirmedFor[event_id]
                    print("valid change for " + user.name + " and event id " + event_id)
                else:
                    print("invalid change to eventsSignedUpFor for user " + user.name + " and event id " + event_id)

            conn = sqlite3.connect(DB_FILE)
            c = conn.cursor()
            sql = "UPDATE users SET JSON = ? WHERE rowid = ?;"
            args = (json.dumps(user.serialize()), user.identity)
            c.execute(sql, args)
            conn.commit()
            conn.close()
            return self.insertUpdate(user.identity, False, user.serialize())

        else:
            original = self.getUserFromUserID(user.identity, user.identity)
            leader = self.getUserFromUserID(user_id, user_id)
            if user.identity == original.identity and user.isOfficer == original.isOfficer:
                for event_id in user.eventsSignedUpFor.keys():
                    if leader.identity in self.getEventFromEventID(int(event_id)).leaders:
                        original.eventsSignedUpFor[event_id] = user.eventsSignedUpFor[event_id]
                        print("valid change for " + user.name + " and event id " + event_id)
                    else:
                        print("invalid change to eventsSignedUpFor for user " + user.name + " and event id " + event_id)
                
                for event_id in original.eventsSignedUpFor.keys():
                    if leader.identity in self.getEventFromEventID(int(event_id)).leaders and event_id not in user.eventsSignedUpFor.keys():
                        del original.eventsSignedUpFor[event_id]

                for event_id in user.eventsConfirmedFor.keys():
                    if leader.identity in self.getEventFromEventID(int(event_id)).leaders:
                        original.eventsConfirmedFor[event_id] = user.eventsConfirmedFor[event_id]
                        print("valid change for " + user.name + " and event id " + event_id)
                    else:
                        print("invalid change to eventsSignedUpFor for user " + user.name + " and event id " + event_id)

                for event_id in original.eventsConfirmedFor.keys():
                    if leader.identity in self.getEventFromEventID(int(event_id)).leaders and event_id not in user.eventsConfirmedFor.keys():
                        del original.eventsConfirmedFor[event_id]

                conn = sqlite3.connect(DB_FILE)
                c = conn.cursor()
                sql = "UPDATE users SET JSON = ? WHERE rowid = ?"
                args = (json.dumps(original.serialize()), user.identity)
                c.execute(sql, args)
                conn.commit()
                conn.close()

                return self.insertUpdate(user.identity, False, user.serialize())

            else:
                print("fuck me")
                return -1
        


    def getEventFromEventID(self, event_id):
        conn = sqlite3.connect(DB_FILE)
        c = conn.cursor()
        sql = "SELECT *, rowid FROM events WHERE rowid = ?;"
        args = (event_id,)
        c.execute(sql, args)
        output = c.fetchone()
        conn.close()

        eventSerialized = json.loads(output[1])
        event = CommServEvent()
        event = event.jsonEventToObject(eventSerialized)
        #event._startTimeMS = json.loads(event._startTimeMS)
        #event.leaders = json.loads(event.leaders)
        #event.files = json.loads(event.files)

        return event

    def updateEvent(self, event, user_id):
        user = self.getUserFromUserID(user_id, user_id)
        if ((user.identity in self.getEventFromEventID(event.identity).leaders) or user.isOfficer > 0) and self.lockStatus(user_id, event.identity):
            #event._startTimeMS = json.dumps(event._startTimeMS)
            #event.files = json.dumps(event.files)
            #event.leaders = json.dumps(event.leaders)

            conn = sqlite3.connect(DB_FILE)
            c = conn.cursor()
            sql = "UPDATE events SET JSON = ? WHERE rowid = ?"
            args = (json.dumps(event.serialize()), event.identity)
            c.execute(sql, args)
            conn.commit()
            conn.close()
            self.releaseLock(user_id, event.identity)
            return self.insertUpdate(event.identity, True, event.serialize())
        else:
            return -1


    def addUser(self, user, newEvents):
        if user.email == "INVALID":
            return "fuck you"
        user.isOfficer = self.officer(user.email)
       
        for dayEvent in user.eventsSignedUpFor.keys():
            if int(dayEvent) < 0:
                import copy
                days = copy.deepcopy(user.eventsSignedUpFor[dayEvent])
                user.eventsSignedUpFor[str(newEvents[-1 * int(dayEvent) - 1])] = days 
                del user.eventsSignedUpFor[dayEvent]

        for dayEvent in user.eventsConfirmedFor.keys():
            if int(dayEvent) < 0:
                import copy
                days = copy.deepcopy(user.eventsConfirmedFor[dayEvent])
                user.eventsConfirmedFor[str(newEvents[-1 * int(dayEvent) - 1])] = days 
                del user.eventsConfirmedFor[dayEvent]


        conn = sqlite3.connect(DB_FILE)
        c = conn.cursor()

        sql = "SELECT rowid FROM users WHERE IDENTITY = (SELECT MAX(IDENTITY)  FROM users);"
        c.execute(sql)
        output = c.fetchone()
        if output is None:
            user.identity = 1
        else:
            user.identity = int(output[0]) + 1

        sql2 = "INSERT INTO users VALUES(?, ?, ?);"
        args = (user.identity, json.dumps(user.serialize()), user.email)
        c.execute(sql2, args)
        user.identity = c.lastrowid
        conn.commit()
        conn.close()
        
        return self.insertUpdate(user.identity, False, user.serialize())

    def addUserWithEmail(self, email, name):
        user = CommServPerson(inEmail = email, inName = name)
        return self.addUser(user, [])


    def addEvent(self, event, user_id):
        if self.getUserFromUserID(user_id, user_id).isOfficer:
            #event._startTimeMS = json.dumps(event._startTimeMS)
            #event.leaders = json.dumps(event.leaders)
            #event.files = json.dumps(event.files)
            
            conn = sqlite3.connect(DB_FILE)
            c = conn.cursor()

            sql = "SELECT * FROM events WHERE IDENTITY = (SELECT MAX(IDENTITY) FROM events);"
            c.execute(sql)
            output = c.fetchone()
            if output is None:
                event.identity = 1
            else:
                event.identity = int(output[0]) + 1

            sql2 = "INSERT INTO events VALUES(?, ?);"
            args = (event.identity, json.dumps(event.serialize()))
            c.execute(sql2, args)
            event.identity = c.lastrowid
            conn.commit()
            conn.close()

            return self.insertUpdate(event.identity, True, event.serialize())
        else:
            return -1

    def insertUpdate(self, identity, isEvent, serialization):
        conn = sqlite3.connect(DB_FILE)
        c = conn.cursor()
        sql = "SELECT COUNT(*) FROM updates"
        c.execute(sql)
        if c.fetchone()[0] != 0:
            sql = "SELECT * FROM updates WHERE UPDATE_IDENTITY = (SELECT MAX(UPDATE_IDENTITY)  FROM updates);"
            c.execute(sql)
            output = c.fetchone()
            updateIdentity = int(output[3]) + 1
        else:
            updateIdentity = 1

        sql = "INSERT INTO updates VALUES(?, ?, ?, ?);"
        args = (str(datetime.datetime.now()), identity, isEvent, updateIdentity)
        c.execute(sql, args)
        conn.commit()
        conn.close()

        return {"update_id" : updateIdentity, "object" : serialization, "type" : identity}

    def getNewData(self, lastUpdated, user_id):
        
        conn = sqlite3.connect(DB_FILE)
        c = conn.cursor()

        minUpdateID = 0
        if lastUpdated >= 1:
            sql = "SELECT * FROM updates WHERE UPDATE_IDENTITY = ?;"
            args = (lastUpdated,)
            c.execute(sql, args)
            minUpdateID = c.fetchone()[3]
        sql = "SELECT * FROM updates WHERE UPDATE_IDENTITY = (SELECT MAX(UPDATE_IDENTITY) FROM updates);"
        c.execute(sql)
        update = c.fetchone()

        if update is None:
            return {"serverData" : {"people" : [], "events" : []}, "user_id" : user_id, "update_id" : 1}
            
        maxUpdateID = update[3]
        dataMax = {
            "serverData": {
                "people": [],
                "events": []
            },
            "user_id": user_id,
            "update_id": maxUpdateID,
            "leaderTimeMultiplier": 2
        }
        for updateID in range(minUpdateID + 1, maxUpdateID + 1):
            sql = "SELECT * FROM updates WHERE UPDATE_IDENTITY = ?;"
            args = (updateID,)
            c.execute(sql, args)
            theData = c.fetchone()
            if theData[2]:
                try:
                    dataMax["serverData"]["events"].remove(self.getEventFromEventID(theData[1]).serialize())
                except ValueError:
                    pass
                dataMax["serverData"]["events"].append(self.getEventFromEventID(theData[1]).serialize())
            else:
                try:
                    dataMax["serverData"]["people"].remove(self.getUserFromUserID(theData[1], user_id).serialize())
                except ValueError:
                    pass
                dataMax["serverData"]["people"].append(self.getUserFromUserID(theData[1], user_id).serialize())

        return dataMax

    def validate(self, email):
        conn = sqlite3.connect(DB_FILE)
        c = conn.cursor()
        sql = "SELECT * FROM users WHERE EMAIL = ?;"
        args = (email,)
        print("=== BEGIN NEW INTERACTION ===")
        print("Validating " + email)
        c.execute(sql, args)
        try:
            email = c.fetchone()[0]
        except:
            return "INVALID"
        return email

    def getUserIDFromEmail(self, email):
        conn = sqlite3.connect(DB_FILE)
        c = conn.cursor()
        sql = "SELECT IDENTITY FROM users WHERE EMAIL = ?;"
        args = (email,)
        c.execute(sql, args)
        return c.fetchone()[0]

    def lockStatus(self, user_id, event_id):
        if (self.getUserFromUserID(user_id, user_id)):
            conn = sqlite3.connect(DB_FILE)
            c = conn.cursor()
            sql = "SELECT * FROM edits WHERE EVENT_ID = ?;"
            args = (event_id,)
            c.execute(sql, args)
            d = c.fetchone()
            if d is None:
                print("no edit matches")
                sql = "INSERT INTO edits VALUES(?, ?, ?);"
                args = (event_id, user_id, str(datetime.datetime.now()),)
                c.execute(sql, args)
                conn.commit()
                conn.close()
                return {"result" : True}
            elif datetime.datetime.strptime(str(d[2]), '%Y-%m-%d %H:%M:%S.%f') + datetime.timedelta(minutes=5) < datetime.datetime.now() or d[1] == user_id:
                print("timeout logic triggered")
                sql = "UPDATE edits SET EVENT_ID = ?, USER_ID = ?, TIME_STARTED = ? WHERE EVENT_ID = ?;"
                args = (event_id, user_id, str(datetime.datetime.now()), event_id,)
                c.execute(sql, args)
                conn.commit()
                conn.close()
                return {"result" : True}
            else:
                return {"result" : False, "person" : self.getUserFromUserID(int(d[1]), int(d[1])).name}
        else:
            return {"result" : False}

    def releaseLock(self, user_id, event_id):
        conn = sqlite3.connect(DB_FILE)
        c = conn.cursor()
        sql = "SELECT * FROM edits WHERE EVENT_ID = ?;"
        args = (event_id,)
        c.execute(sql, args)
        d = c.fetchone()
        if d is not None and (d[0] == event_id and d[1] == user_id):
            sql = "DELETE FROM edits WHERE EVENT_ID = ?;"
            args = (int(event_id),)
            c.execute(sql, args)
            print("deleted lock from edits")
            conn.commit()
            conn.close()
            return {"result" : True}
        elif d is None:
            return {"result" : True}
        else:
            return {"result" : False}

    def getUpdateID(self):
        conn = sqlite3.connect(DB_FILE)
        c = conn.cursor()
        sql = "SELECT * FROM updates WHERE UPDATE_IDENTITY = (SELECT MAX(UPDATE_IDENTITY) FROM updates);"
        c.execute(sql)
        return c.fetchone()[3]
        
    def getHoursTable(self, user_id):
        user = self.getUserFromUserID(user_id, user_id)
        table = [["Event", "Start Time", "EndTime", "Hours"]]
        for event_id in user.eventsConfirmedFor:
            event_id = int(event_id)
            event = self.getEventFromEventID(int(event_id))
            for dayID in user.eventsConfirmedFor[str(event_id)]:
                line = [event.name]
                if event._startTimeMS[dayID]["deleted"]:
                    continue
                startTime = event._startTimeMS[dayID]["startTimeMS"]
                endTime = event._startTimeMS[dayID]["endTimeMS"]
                line.append(str(datetime.datetime.fromtimestamp(float(startTime) / 1000.0)))
                line.append(str(datetime.datetime.fromtimestamp(float(endTime) / 1000.0)))
                minutes = int(float(endTime - startTime) / (60000.0))
                if minutes % 15 < 7.5:
                    minutes = 15 * (int(minutes) /15)
                else:
                    minutes = 15 * (int(minutes) /15) + 15
                
                minutes = float(minutes) / 60.0
                
                if user_id in event.leaders:
                    minutes = minutes * 2
                line.append(minutes)
                if user_id in event.leaders:
                    line.append("LEADER")
                table.append(line)
        
        table.append(user.name)        
        return table

    def getHoursSuperTable(self, user_id):
        conn = sqlite3.connect(DB_FILE)
        c = conn.cursor()
        sql = "SELECT * FROM users WHERE IDENTITY = (SELECT MAX(IDENTITY)  FROM users);"
        c.execute(sql)
        maximum = c.fetchone()[0]

        table = []
        for x in range(1, maximum + 1):
            table.append(self.getHoursTable(x))
        return table

    def getHoursSuperTable2(self, user_id):
        conn = sqlite3.connect(DB_FILE)
        c = conn.cursor()
        sql = "SELECT * FROM users WHERE IDENTITY = (SELECT MAX(IDENTITY)  FROM users);"
        c.execute(sql)
        maximum = c.fetchone()[0]

	table = []	

        for x in range(1, maximum + 1):
            table.append(self.getHoursTable(x))
        return table

    def upToDate(self, identity, objType, updateID):
        conn = sqlite3.connect(DB_FILE)
        c = conn.cursor()
        sql = "SELECT * FROM updates WHERE IDENTITY = ? and TYPE_OBJECT = ?;"
        args = (int(identity), bool(objType),)
        c.execute(sql, args)
        output = c.fetchall()
        
        maxID = 1
        for row in output:
            if int(row[3]) > maxID:
                maxID = row[3]
        if (maxID -1) > updateID:
            return False
        else:
            return True
