import json

class CommServPerson:

    def __init__(self, inIdentity=0, inName="", inIsOfficer=0, inEmail="", inEventsSignedUpFor={}, inEventsConfirmedFor={}, inGradYear=None, inStudentID = None, inBirthday = None, inPhone = None):
        self.identity = inIdentity
        self.name = inName
        self.isOfficer = inIsOfficer
        self.email = inEmail
        self.eventsSignedUpFor = inEventsSignedUpFor #List of IDs to avoid recursion
        self.eventsConfirmedFor = inEventsConfirmedFor #List of IDs to avoid recursion
        self.gradYear = inGradYear
        self.studentID = inStudentID
        self.birthday = inBirthday
        self.phone = inPhone

    def jsonPersonToObject(self, user = {}):
        identity = user["id"]
        name = user["name"]
        isOfficer = user["isOfficer"]
        email = user["email"]
        eventsSignedUpFor = user["eventsSignedUpFor"]
        eventsConfirmedFor = user["eventsConfirmedFor"]
        gradYear = user["gradYear"]
        studentID = user["studentID"]
        birthday = user["birthday"]
        phone = user["phone"]
       
        return CommServPerson(identity, name, isOfficer, email, eventsSignedUpFor, eventsConfirmedFor, gradYear, studentID, birthday, phone)

    def serialize(self):
        return {"id": self.identity, "name": self.name, "isOfficer": int(self.isOfficer), "email": self.email, "gradYear": self.gradYear, "eventsSignedUpFor" : self.eventsSignedUpFor, "eventsConfirmedFor" : self.eventsConfirmedFor, "gradYear" : self.gradYear, "studentID" : self.studentID, "birthday" : self.birthday, "phone" : self.phone}
