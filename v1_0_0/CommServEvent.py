from Location import Location


class CommServEvent:
    def __init__(self, inIdentity=-1, in_startTimeMS=0.0, inName="", inDescription="", inEventLoc=Location(), inLeaders=[0], inParkingInfo = "", inIsHidden = False, inFiles = [], inLimit = -1):
        self.identity = inIdentity
        self._startTimeMS = in_startTimeMS
        self.name = inName
        self.description = inDescription
        self.eventLoc = inEventLoc
        self.leaders = inLeaders #List of IDs to avoid recursion
        self.parkingInfo = inParkingInfo
        self.isHidden = int(inIsHidden)
        self.files = inFiles
        self.limit = inLimit
        
    def jsonEventToObject(self, event):
        identity = event["id"]
        _startTimeMS = event["eventDays"]
        name = event["name"]
        description = event["description"]
        eventLoc = Location()
        eventLoc = eventLoc.jsonLocationToObject(event["parkingLocation"])
        leaders = event["leaders"]
        parkingInfo = event["parkingInfo"]
        isHidden = int(event["isHidden"])
        files = event["files"]
        limit = event["limit"]
        
        return CommServEvent(identity, _startTimeMS, name, description, eventLoc, leaders, parkingInfo, isHidden, files, limit)

    def serialize(self):
        return { "id": self.identity, "eventDays": (self._startTimeMS), "name": self.name, "description": self.description, "parkingLocation": self.eventLoc.serialize(), "parkingInfo": self.parkingInfo, "leaders": self.leaders, "isHidden" : bool(self.isHidden), "parkingInfo" : self.parkingInfo, "files" : self.files, "limit" : self.limit}
